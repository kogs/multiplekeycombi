/**
 *
 */
package de.kogs.javafx.multipleKeyCombination;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 * @author <a href="mailto:marcel.vogel@proemion.com">mv1015</a>
 *
 */
public class TestApp extends Application {
	
	public static void main(String[] args) {
		launch(args);
	}
	
	/* (non-Javadoc)
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	@Override
	public void start(Stage primaryStage) throws Exception {
		
		MenuBar bar = new MenuBar();
		
		Menu menu = new Menu("Test");
		
		menu.setAccelerator(new MultipleKeyCombination(KeyCode.A));
		
		bar.getMenus().add(menu);
		
		MenuItem item = new MenuItem("Hallo");
		item.setAccelerator(new MultipleKeyCombination(KeyCode.A,KeyCode.S));
		item.setOnAction((event)->{
			System.out.println("Hallo");
		});
		
		
		menu.getItems().add(item);
		
		
		
		
		
		
		
		StackPane root = new StackPane(bar);
		
		
		
		Scene scene = new Scene(root);
		MultipleKeyCombination.setupMultipleKeyCombination(scene);
		primaryStage.setScene(scene);
		primaryStage.show();
		
	}
	
}
